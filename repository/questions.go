package repository

import (
	"code_crack_be/domain"

	"gorm.io/gorm"
)

type QuestionRepository struct {
	DB *gorm.DB
}

// FindAll implements domain.QuestionRepository.
func (q *QuestionRepository) FindAll(param map[string]any) ([]domain.Question, error) {
	var questions []domain.Question

	err := q.DB.Where(param).Find(&questions).Error
	if err != nil {
		return nil, err
	}

	return questions, nil
}

func NewQuestionRepository(db *gorm.DB) domain.QuestionRepository {
	return &QuestionRepository{DB: db}
}
