package service

import (
	"code_crack_be/domain"
	"time"

	"golang.org/x/exp/rand"
)

type QuestionService struct {
	QuestionRepo domain.QuestionRepository
}

// FindAll implements domain.QuestionService.
func (q *QuestionService) FindAll(param map[string]any) (*domain.Questions, error) {
	data, err := q.QuestionRepo.FindAll(param)
	if err != nil {
		return nil, err
	}

	// Limit 5 data and randomize
	rand.Seed(uint64(time.Now().Unix()))
	rand.Shuffle(len(data), func(i, j int) {
		data[i], data[j] = data[j], data[i]
	})
	data = data[:5]

	questions := make([][]string, len(data))

	for i, d := range data {
		var options string

		for _, o := range d.Options {
			options += o

			if o != d.Options[len(d.Options)-1] {
				options += "|"
			}
		}

		questions[i] = []string{d.Level, d.Question, options, d.Answer}
	}

	return &domain.Questions{Questions: questions}, nil
}

func NewQuestionService(questionRepo domain.QuestionRepository) domain.QuestionService {
	return &QuestionService{
		QuestionRepo: questionRepo,
	}
}
