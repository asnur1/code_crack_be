package handler

import (
	"code_crack_be/domain"
	"encoding/json"

	"github.com/gofiber/fiber/v2"
)

type QuestionHandler struct {
	QuestionService domain.QuestionService
}

func NewQuestionHandler(questionService domain.QuestionService) *QuestionHandler {
	return &QuestionHandler{
		QuestionService: questionService,
	}
}

func (q *QuestionHandler) GetAll(c *fiber.Ctx) error {
	var param map[string]any

	level := c.Query("level")

	if level != "" {
		param = map[string]any{
			"level": level,
		}
	}

	questions, err := q.QuestionService.FindAll(param)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	jsonByte, _ := json.Marshal(questions)

	return c.Status(fiber.StatusOK).SendString(string(jsonByte))
}
