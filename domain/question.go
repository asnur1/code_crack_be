package domain

import "github.com/lib/pq"

type Question struct {
	ID       int            `json:"id" gorm:"primaryKey"`
	Question string         `json:"question" gorm:"not null;column:question"`
	Options  pq.StringArray `json:"options" gorm:"not null;column:options;type:text[]"`
	Answer   string         `json:"answer" gorm:"not null;column:answer"`
	Level    string         `json:"level" gorm:"not null;column:level"`
}

type Questions struct {
	Questions [][]string `json:"Questions"`
}

func (*Question) TableName() string {
	return "quiz"
}

type QuestionRepository interface {
	FindAll(param map[string]any) ([]Question, error)
}

type QuestionService interface {
	FindAll(param map[string]any) (*Questions, error)
}
