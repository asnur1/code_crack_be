package config

import (
	"os"

	"github.com/joho/godotenv"
)

func InitializeConfig(files ...string) error {
	// Loaf config from files
	err := godotenv.Load(files...)

	_, exist := os.LookupEnv("DB_HOST")

	if !exist {
		if err != nil {
			return err
		}
	}

	return nil
}
