--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Debian 12.17-1.pgdg120+1)
-- Dumped by pg_dump version 12.17 (Debian 12.17-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: quiz; Type: TABLE; Schema: public; Owner: game
--

CREATE TABLE public.quiz (
    id integer NOT NULL,
    question text NOT NULL,
    options text[] NOT NULL,
    answer character varying(255) NOT NULL,
    level character varying(50) NOT NULL
);


ALTER TABLE public.quiz OWNER TO game;

--
-- Name: quiz_id_seq; Type: SEQUENCE; Schema: public; Owner: game
--

CREATE SEQUENCE public.quiz_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.quiz_id_seq OWNER TO game;

--
-- Name: quiz_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: game
--

ALTER SEQUENCE public.quiz_id_seq OWNED BY public.quiz.id;


--
-- Name: quiz id; Type: DEFAULT; Schema: public; Owner: game
--

ALTER TABLE ONLY public.quiz ALTER COLUMN id SET DEFAULT nextval('public.quiz_id_seq'::regclass);


--
-- Data for Name: quiz; Type: TABLE DATA; Schema: public; Owner: game
--

COPY public.quiz (id, question, options, answer, level) FROM stdin;
1	Apa itu variabel dalam pemrograman?	{"Sebuah nilai tetap yang tidak bisa diubah","Sebuah nama yang mewakili suatu nilai yang bisa diubah","Sebuah fungsi yang mengembalikan nilai","Sebuah komentar dalam kode"}	Sebuah nama yang mewakili suatu nilai yang bisa diubah	newbie
2	Manakah dari berikut ini yang merupakan bahasa pemrograman?	{HTML,CSS,JavaScript,SQL}	JavaScript	newbie
3	Apa yang dimaksud dengan statement if dalam pemrograman?	{"Menampilkan output ke layar","Mengeksekusi kode berdasarkan kondisi yang diberikan","Menghentikan program","Mendefinisikan variabel baru"}	Mengeksekusi kode berdasarkan kondisi yang diberikan	newbie
4	Apa fungsi dari loop dalam pemrograman?	{"Menyimpan data","Mengeksekusi blok kode berulang kali","Memeriksa kesalahan dalam kode","Menutup program"}	Mengeksekusi blok kode berulang kali	newbie
5	Apa perbedaan antara = dan == dalam banyak bahasa pemrograman?	{"= digunakan untuk membandingkan dua nilai, == digunakan untuk menetapkan nilai","= digunakan untuk menetapkan nilai, == digunakan untuk membandingkan dua nilai","Keduanya memiliki arti yang sama","= digunakan untuk penjumlahan, == digunakan untuk pengurangan"}	= digunakan untuk menetapkan nilai	newbie
6	Apa itu fungsi dalam pemrograman?	{"Sebuah variabel yang menyimpan nilai tetap","Sebuah blok kode yang melakukan tugas tertentu dan dapat dipanggil kembali","Sebuah perintah untuk menghapus file","Sebuah metode untuk mendebug program"}	Sebuah blok kode yang melakukan tugas tertentu dan dapat dipanggil kembali	newbie
7	Apa yang dimaksud dengan komentar dalam kode?	{"Kode yang akan dieksekusi oleh program","Kode yang digunakan untuk memberikan informasi atau penjelasan dan tidak dieksekusi","Kode yang menghasilkan kesalahan","Kode yang menjalankan perulangan"}	Kode yang digunakan untuk memberikan informasi atau penjelasan dan tidak dieksekusi	newbie
8	Apa hasil dari operasi 5 + 3 * 2 dalam banyak bahasa pemrograman?	{16,11,10,13}	13	newbie
9	Manakah dari berikut ini yang merupakan tipe data dasar dalam banyak bahasa pemrograman?	{"String, Boolean, Function","Integer, Boolean, String","Variable, Integer, Loop","Array, Loop, Function"}	Integer, Boolean, String	newbie
10	Apa tujuan dari debugging dalam pemrograman?	{"Menambahkan fitur baru ke program","Mengoptimalkan kinerja program","Memperbaiki kesalahan atau bug dalam kode","Meningkatkan tampilan antarmuka pengguna"}	Memperbaiki kesalahan atau bug dalam kode	newbie
\.


--
-- Name: quiz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: game
--

SELECT pg_catalog.setval('public.quiz_id_seq', 10, true);


--
-- Name: quiz quiz_pkey; Type: CONSTRAINT; Schema: public; Owner: game
--

ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT quiz_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

