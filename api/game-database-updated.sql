--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Debian 12.17-1.pgdg120+1)
-- Dumped by pg_dump version 12.17 (Debian 12.17-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: quiz; Type: TABLE; Schema: public; Owner: game
--

CREATE TABLE public.quiz (
    id integer NOT NULL,
    question text NOT NULL,
    options text[] NOT NULL,
    answer character varying(255) NOT NULL,
    level character varying(50) NOT NULL
);


ALTER TABLE public.quiz OWNER TO game;

--
-- Name: quiz_id_seq; Type: SEQUENCE; Schema: public; Owner: game
--

CREATE SEQUENCE public.quiz_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.quiz_id_seq OWNER TO game;

--
-- Name: quiz_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: game
--

ALTER SEQUENCE public.quiz_id_seq OWNED BY public.quiz.id;


--
-- Name: quiz id; Type: DEFAULT; Schema: public; Owner: game
--

ALTER TABLE ONLY public.quiz ALTER COLUMN id SET DEFAULT nextval('public.quiz_id_seq'::regclass);


--
-- Data for Name: quiz; Type: TABLE DATA; Schema: public; Owner: game
--

COPY public.quiz (id, question, options, answer, level) FROM stdin;
1	Apa itu variabel dalam pemrograman?	{"Sebuah nilai tetap yang tidak bisa diubah","Sebuah nama yang mewakili suatu nilai yang bisa diubah","Sebuah fungsi yang mengembalikan nilai","Sebuah komentar dalam kode"}	Sebuah nama yang mewakili suatu nilai yang bisa diubah	newbie
2	Manakah dari berikut ini yang merupakan bahasa pemrograman?	{HTML,CSS,JavaScript,SQL}	JavaScript	newbie
3	Apa yang dimaksud dengan statement if dalam pemrograman?	{"Menampilkan output ke layar","Mengeksekusi kode berdasarkan kondisi yang diberikan","Menghentikan program","Mendefinisikan variabel baru"}	Mengeksekusi kode berdasarkan kondisi yang diberikan	newbie
4	Apa fungsi dari loop dalam pemrograman?	{"Menyimpan data","Mengeksekusi blok kode berulang kali","Memeriksa kesalahan dalam kode","Menutup program"}	Mengeksekusi blok kode berulang kali	newbie
5	Apa perbedaan antara = dan == dalam banyak bahasa pemrograman?	{"= digunakan untuk membandingkan dua nilai, == digunakan untuk menetapkan nilai","= digunakan untuk menetapkan nilai, == digunakan untuk membandingkan dua nilai","Keduanya memiliki arti yang sama","= digunakan untuk penjumlahan, == digunakan untuk pengurangan"}	= digunakan untuk menetapkan nilai	newbie
6	Apa itu fungsi dalam pemrograman?	{"Sebuah variabel yang menyimpan nilai tetap","Sebuah blok kode yang melakukan tugas tertentu dan dapat dipanggil kembali","Sebuah perintah untuk menghapus file","Sebuah metode untuk mendebug program"}	Sebuah blok kode yang melakukan tugas tertentu dan dapat dipanggil kembali	newbie
7	Apa yang dimaksud dengan komentar dalam kode?	{"Kode yang akan dieksekusi oleh program","Kode yang digunakan untuk memberikan informasi atau penjelasan dan tidak dieksekusi","Kode yang menghasilkan kesalahan","Kode yang menjalankan perulangan"}	Kode yang digunakan untuk memberikan informasi atau penjelasan dan tidak dieksekusi	newbie
8	Apa hasil dari operasi 5 + 3 * 2 dalam banyak bahasa pemrograman?	{16,11,10,13}	13	newbie
9	Manakah dari berikut ini yang merupakan tipe data dasar dalam banyak bahasa pemrograman?	{"String, Boolean, Function","Integer, Boolean, String","Variable, Integer, Loop","Array, Loop, Function"}	Integer, Boolean, String	newbie
10	Apa tujuan dari debugging dalam pemrograman?	{"Menambahkan fitur baru ke program","Mengoptimalkan kinerja program","Memperbaiki kesalahan atau bug dalam kode","Meningkatkan tampilan antarmuka pengguna"}	Memperbaiki kesalahan atau bug dalam kode	newbie
11	Apa output dari kode berikut ini?\\n\\nx = 10\\ny = 5\\nprint(x + y * 2)	{30,20,15,25}	15	junior
12	Apa output dari kode berikut ini?\\n\\nnumbers = [1, 2, 3, 4, 5]\\nprint(numbers[2])	{1,2,3,4}	3	junior
13	Bagaimana cara menambahkan elemen ke dalam list di Python?	{list.append(element),list.add(element),list.insert(element),list.add_element(element)}	list.append(element)	junior
14	Apa output dari kode berikut ini?\\n\\nfor i in range(3):\\n    print(i)	{"1 2 3","0 1 2","0 1 2 3","1 2"}	0 1 2	junior
15	Apa output dari kode berikut ini?\\n\\nx = 5\\nif x > 2:\\n    print("Greater than 2")\\nelse:\\n    print("2 or less")	{"2 or less","Greater than 2","No output",Error}	Greater than 2	junior
16	Bagaimana cara mengimpor modul di Python?	{"import module_name","include module_name","require module_name","use module_name"}	import module_name	junior
17	Apa output dari kode berikut ini?\\n\\ndef add(a, b):\\n    return a + b\\n\\nresult = add(3, 4)\\nprint(result)	{7,34,12,5}	7	junior
18	Apa output dari kode berikut ini?\\n\\nx = 10\\ny = "20"\\nprint(x + int(y))	{1020,30,Error,20}	30	junior
19	Apa output dari kode berikut ini?\\n\\nx = [1, 2, 3, 4]\\nx.append(5)\\nprint(x)	{"[1, 2, 3, 4]","[1, 2, 3, 4, 5]","[1, 2, 3, 4, 5, 6]","[5, 1, 2, 3, 4]"}	[1, 2, 3, 4, 5]	junior
20	Bagaimana cara menangkap exception di Python?\\n\\ntry: ... except: ...\\ntry: ... catch: ...\\ncatch: ... try: ...\\nbegin: ... rescue: ...	{"try: ... except: ...","try: ... catch: ...","catch: ... try: ...","begin: ... rescue: ..."}	try: ... except: ...	junior
21	Apa output dari kode berikut ini?\\n\\nx = 10\\nwhile x > 0:\\n    print(x)\\n    x -= 2	{"10 8 6 4 2 0","10 9 8 7 6 5 4 3 2 1","10 8 6 4 2","10 8 6 4 2 -2"}	10 8 6 4 2	junior
22	Apa output dari kode berikut ini?\\n\\nmy_dict = {"a": 1, "b": 2, "c": 3}\\nprint(my_dict["b"])	{a,1,2,3}	2	junior
23	Apa output dari kode berikut ini?\\n\\nx = 5\\ny = 10\\nz = x * y\\nif z > 40:\\n    print("z is greater than 40")\\nelse:\\n    print("z is 40 or less")	{"z is greater than 40","z is 40 or less",50,Error}	z is greater than 40	junior
24	Apa output dari kode berikut ini?\\n\\ndef factorial(n):\\n    if n == 0:\\n        return 1\\n    else:\\n        return n * factorial(n-1)\\n\\nprint(factorial(5))	{10,120,25,30}	120	middle
25	Apa perbedaan antara list dan tuple dalam Python?	{"list bisa diubah (mutable), tuple tidak bisa diubah (immutable)","list bisa diurutkan (sorted), tuple tidak bisa diurutkan (unsorted)","list bisa diindeks (indexed), tuple tidak bisa diindeks (unindexed)","Tidak ada perbedaan antara keduanya"}	list bisa diubah (mutable), tuple tidak bisa diubah (immutable)	middle
26	Apa output dari kode berikut ini?\\n\\nx = [1, 2, 3]\\ny = x\\ny.append(4)\\nprint(x)	{"[1, 2, 3]","[1, 2, 3, 4]","[1, 2, 4]",Error}	[1, 2, 3, 4]	middle
27	Bagaimana cara melakukan pengurutan (sorting) pada dictionary berdasarkan nilai (value) di Python?\\n\\nsorted(d.items(), key=lambda x: x[1])\\nd.sort(key=lambda x: x[1])\\nd.sort_by_value()\\nTidak mungkin mengurutkan dictionary berdasarkan nilai	{"sorted(d.items(), key=lambda x: x[1])","d.sort(key=lambda x: x[1])",d.sort_by_value(),"Tidak mungkin mengurutkan dictionary berdasarkan nilai"}	sorted(d.items()	middle
28	Apa yang dilakukan oleh fungsi map dalam Python?	{"Mengubah tipe data","Memetakan fungsi ke setiap elemen dalam list","Menghitung panjang sebuah list","Mengurutkan list"}	Memetakan fungsi ke setiap elemen dalam list	middle
29	Apa yang dimaksud dengan JOIN dalam SQL?	{"Menggabungkan dua atau lebih tabel berdasarkan kondisi tertentu","Menghapus data dari sebuah tabel","Menyisipkan data baru ke dalam sebuah tabel","Memilih data dari sebuah tabel"}	Menggabungkan dua atau lebih tabel berdasarkan kondisi tertentu	middle
30	Bagaimana cara menghapus semua data dari sebuah tabel di SQL?	{"DELETE * FROM table_name;","TRUNCATE TABLE table_name;","REMOVE FROM table_name;","DROP TABLE table_name;"}	TRUNCATE TABLE table_name;	middle
31	Apa yang dimaksud dengan UNION dan UNION ALL dalam SQL?	{"Operasi untuk menggabungkan dua atau lebih tabel","Operasi untuk menghitung total baris pada dua atau lebih tabel","Operasi untuk mengurutkan hasil query","Operasi untuk menghapus duplikat dari hasil query"}	Operasi untuk menggabungkan dua atau lebih tabel	middle
32	Apa output dari query berikut?\\n\\nsql\\nCopy code\\nSELECT AVG(salary)\\nFROM employees\\nWHERE department = 'IT';	{"Rata-rata gaji dari seluruh karyawan","Rata-rata gaji dari karyawan di departemen IT","Rata-rata gaji dari karyawan yang bergabung setelah tahun 2020","Error karena sintaks SQL salah"}	Rata-rata gaji dari karyawan di departemen IT	middle
33	Apa yang dimaksud dengan INDEX dalam SQL?	{"Sebuah nilai tunggal yang unik di setiap baris","Sebuah tipe data khusus untuk mengindeks kolom","Sebuah struktur data yang menyimpan kunci asing","Sebuah struktur data yang meningkatkan kecepatan pencarian data di kolom tertentu"}	Sebuah struktur data yang meningkatkan kecepatan pencarian data di kolom tertentu	middle
34	Apa perbedaan antara INNER JOIN dan LEFT JOIN dalam SQL?	{"INNER JOIN hanya mengembalikan baris yang memiliki nilai yang cocok di kedua tabel, sedangkan LEFT JOIN mengembalikan semua baris dari tabel kiri dan baris yang cocok dari tabel kanan (jika ada)","INNER JOIN mengembalikan semua baris dari tabel kiri dan baris yang cocok dari tabel kanan, sedangkan LEFT JOIN hanya mengembalikan baris yang memiliki nilai yang cocok di kedua tabel","INNER JOIN hanya mengembalikan baris yang memiliki nilai yang cocok di kedua tabel, sedangkan LEFT JOIN mengembalikan semua baris dari kedua tabel","Tidak ada perbedaan antara keduanya"}	INNER JOIN hanya mengembalikan baris yang memiliki nilai yang cocok di kedua tabel, sedangkan LEFT JOIN mengembalikan semua baris dari tabel kiri dan baris yang cocok dari tabel kanan (jika ada)	middle
35	Apa yang dimaksud dengan generator expression dalam Python?	{"Ekspresi yang menghasilkan generator","Fungsi yang menghasilkan ekspresi matematika","Ekspresi yang menghasilkan nilai tunggal","Fungsi yang mengembalikan list"}	Ekspresi yang menghasilkan generator	senior
36	Apa yang dimaksud dengan generator expression dalam Python?	{"Ekspresi yang menghasilkan generator","Fungsi yang menghasilkan ekspresi matematika","Ekspresi yang menghasilkan nilai tunggal","Fungsi yang mengembalikan list"}	Ekspresi yang menghasilkan generator	senior
37	Apa yang dilakukan oleh fungsi functools.partial dalam Python?	{"Membuat salinan objek secara rekursif","Mengembalikan salinan mendalam dari objek","Menetapkan beberapa argumen dari fungsi ke nilai tetap","Memanggil fungsi dengan satu argumen saja"}	Menetapkan beberapa argumen dari fungsi ke nilai tetap	senior
38	Apa itu metaclass dalam Python?	{"Kelas yang mewarisi kelas lain, Kelas dari kelas","Kelas yang menggunakan decorators khusus","Kelas yang hanya bisa dibuat satu instansinya"}	Kelas dari kelas	senior
39	Bagaimana cara mengatasi global interpreter lock (GIL) dalam Python?	{"Dengan menggunakan multiprocessing daripada multithreading","Dengan memanggil fungsi unlock_gil() sebelum operasi yang membutuhkan GIL","Dengan menetapkan prioritas tinggi pada thread yang membutuhkan GIL","Tidak ada cara untuk mengatasi GIL"}	Dengan menggunakan multiprocessing daripada multithreading	senior
40	Apa yang dilakukan oleh metode __slots__ dalam definisi kelas di Python?	{"Menyimpan nilai-nilai dari objek kelas, Membuat salinan objek kelas","Membatasi atribut-atribut yang bisa dimiliki oleh objek kelas","Membuat kelas yang hanya bisa memiliki satu instansi"}	Membatasi atribut-atribut yang bisa dimiliki oleh objek kelas	senior
41	Apa yang dilakukan oleh fungsi os.path.join() dalam modul os di Python?	{"Menggabungkan dua atau lebih path menjadi satu","Menghapus path dari sistem operasi","Memperbarui path ke versi terbaru","Tidak ada fungsi tersebut di modul os"}	Menggabungkan dua atau lebih path menjadi satu	senior
42	Apa yang dimaksud dengan descriptor dalam Python?	{"Fungsi yang mengembalikan nilai dengan menggunakan return","Objek yang mendefinisikan cara akses ke atribut kelas","Dekorator yang digunakan untuk menambahkan fitur pada metode","Fungsi yang hanya menerima satu argumen"}	Objek yang mendefinisikan cara akses ke atribut kelas	senior
43	Bagaimana cara menangani penggunaan memori yang efisien dalam Python?	{"Dengan menggunakan garbage collection secara manual","Dengan menghapus objek secara eksplisit menggunakan delete, Dengan membatasi penggunaan mutable objects","Dengan menggunakan gc.collect() secara teratur"}	Dengan membatasi penggunaan mutable objects	senior
44	Apa yang dilakukan oleh decorator @staticmethod dalam Python?	{"Membuat metode statis dalam kelas","Menggabungkan dua kelas menjadi satu","Membatasi akses ke metode dalam kelas","Tidak ada decorator tersebut di Python"}	Membuat metode statis dalam kelas	senior
45	Apa yang dimaksud dengan method resolution order (MRO) dalam Python?	{"Urutan pemanggilan metode dalam kelas","Urutan penentuan nama metode dalam kelas","Urutan penentuan prioritas antar kelas","Urutan resolusi imports dalam kode Python"}	Urutan pemanggilan metode dalam kelas	senior
46	Apa yang dilakukan oleh fungsi filter dalam Python?	{"Mengurutkan elemen-elemen dalam list","Membuat salinan dalam deep copy dari list","Memfilter elemen-elemen dalam list berdasarkan fungsi tertentu","Menggabungkan dua list menjadi satu"}	Memfilter elemen-elemen dalam list berdasarkan fungsi tertentu	senior
47	Apa yang dimaksud dengan lambda function dalam Python?	{"Fungsi yang didefinisikan dengan kata kunci lambda","Fungsi yang memiliki parameter lebih dari satu","Fungsi yang mengembalikan nilai secara bertahap","Fungsi yang mengurutkan elemen dalam list"}	Fungsi yang didefinisikan dengan kata kunci lambda	senior
48	Apa output dari kode berikut ini?\\n\\ndef outer_function(x):\\n    def inner_function(y):\\n        return x + y\\n    return inner_function\\n\\nadd_five = outer_function(5)\\nprint(add_five(3))\\n	{5,8,3,15}	8	senior
49	Apa perbedaan antara class method dan static method dalam Python?	{"Class method tidak dapat diakses oleh instance kelas","sedangkan static method dapat diakses oleh instance kelas","Class method dapat diakses tanpa membuat instance kelas","sedangkan static method tidak dapat diakses tanpa membuat instance kelas","Tidak ada perbedaan antara keduanya","Class method dapat mengakses atribut kelas","sedangkan static method tidak dapat mengakses atribut kelas"}	Class method dapat diakses tanpa membuat instance kelas, sedangkan static method tidak dapat diakses tanpa membuat instance kelas	senior
50	Apa yang dilakukan oleh fungsi reduce dalam modul functools di Python?	{"Menggabungkan dua list menjadi satu","Mengurutkan elemen-elemen dalam list","Memanggil fungsi pada setiap elemen dalam list","Mereduksi list menjadi satu nilai menggunakan fungsi tertentu"}	Mereduksi list menjadi satu nilai menggunakan fungsi tertentu	senior
51	Apa yang dilakukan oleh try dan finally dalam blok try-finally di Python?	{"Menjalankan blok kode tanpa menangkap eksepsi","Menjalankan blok kode dan menangkap eksepsi yang spesifik","Menjalankan blok kode dan selalu mengeksekusi blok kode finally terlepas dari apakah terjadi eksepsi atau tidak","Menjalankan blok kode tanpa mengeksekusi blok kode finally"}	Menjalankan blok kode dan selalu mengeksekusi blok kode finally terlepas dari apakah terjadi eksepsi atau tidak	senior
52	Apa yang dilakukan oleh operator yield dalam Python?	{"Mengembalikan nilai dari sebuah fungsi generator","Menambahkan nilai ke dalam list","Menetapkan nilai ke sebuah variabel","Menghapus nilai dari sebuah list"}	Mengembalikan nilai dari sebuah fungsi generator	senior
53	Bagaimana cara mengatasi name mangling dalam Python?	{"Menggunakan metode _method_name","Menggunakan metode __method_name","Menggunakan metode ___method_name___","Tidak ada cara untuk mengatasi name mangling"}	Menggunakan metode __method_name	senior
54	Apa yang dimaksud dengan context manager dalam Python?	{"Objek yang mengatur konteks penggunaan sumber daya seperti file atau koneksi database","Objek yang mengatur konteks pembuatan instance kelas","Objek yang mengatur konteks penggunaan decorators","Objek yang mengatur konteks penggunaan lambda functions"}	Objek yang mengatur konteks penggunaan sumber daya seperti file atau koneksi database	senior
\.


--
-- Name: quiz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: game
--

SELECT pg_catalog.setval('public.quiz_id_seq', 54, true);


--
-- Name: quiz quiz_pkey; Type: CONSTRAINT; Schema: public; Owner: game
--

ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT quiz_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

