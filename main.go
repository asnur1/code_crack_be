package main

import (
	"code_crack_be/config"
	"code_crack_be/handler"
	"code_crack_be/repository"
	"code_crack_be/service"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	// Initialize Config
	err := config.InitializeConfig(".env")

	if err != nil {
		panic(err)
	}

	// Connect to DB
	db, err := config.Connect()

	if err != nil {
		panic(err)
	}

	// Initialize Repository
	questionRepo := repository.NewQuestionRepository(db)

	// Initialize Service
	questionService := service.NewQuestionService(questionRepo)

	// Initialize Handler
	questionHandler := handler.NewQuestionHandler(questionService)

	// Initialize Fiber
	app := fiber.New()

	// CORS
	app.Use(cors.New())

	// Route
	app.Get("/health", func(c *fiber.Ctx) error {
		return c.SendString("OK")
	})

	app.Get("/quiz", questionHandler.GetAll)

	// Start Server
	app.Listen(":3000")
}
